# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#
# Package building GeoModelCore Kernel for ATLAS.
#

# Set a minimum required CMake version to use.
cmake_minimum_required( VERSION 3.7 )

# Make sure that all _ROOT variables *are* used when they are set.
if( POLICY CMP0074 )
   cmake_policy( SET CMP0074 NEW )
endif()

# The name of the package:
atlas_subdir( GeoModelCore )

# In release recompilation mode finish here:
if( ATLAS_RELEASE_MODE )
   return()
endif()

if( ATLAS_BUILD_EIGEN )
   set( EIGEN_INCLUDE_DIRS "${CMAKE_INCLUDE_OUTPUT_DIRECTORY}/eigen3" )
else()
   find_package( Eigen 3.0.5 REQUIRED )
endif()

set( _extraOptions )
if ( "${CMAKE_BUILD_TYPE}" STREQUAL "Debug" )
    # Some functions here make heavy use of Eigen.
    # Compiling them without optimization is just too slow.
    list( APPEND _extraOptions -DCMAKE_BUILD_TYPE:STRING=RelWithDebInfo )
elseif( NOT "${CMAKE_BUILD_TYPE}" STREQUAL "" )
    list( APPEND _extraOptions -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE} )
endif()

if( "${CMAKE_CXX_STANDARD}" GREATER_EQUAL 11 )
   list( APPEND _extraOptions -DCMAKE_CXX_STANDARD:STRING=${CMAKE_CXX_STANDARD} )
endif()

# Directory for the temporary build results:
set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/GeoModelCoreBuild )

# Set up the build of GeoModelCore:
ExternalProject_Add( GeoModelCore
  PREFIX ${CMAKE_BINARY_DIR}
  INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
  GIT_REPOSITORY https://gitlab.cern.ch/GeoModelDev/GeoModelCore.git
  GIT_TAG 3.1.2
  CMAKE_CACHE_ARGS
  -DCMAKE_INSTALL_LIBDIR:PATH=${CMAKE_INSTALL_LIBDIR}
  -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
  -DEIGEN3_INCLUDE_DIR:PATH=${EIGEN_INCLUDE_DIRS}
  ${_extraOptions}
  LOG_CONFIGURE 1
)
ExternalProject_Add_Step( GeoModelCore buildinstall
  COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/ <INSTALL_DIR>
  COMMENT "Installing GeoModelCore into the build area"
  DEPENDEES install  )
ExternalProject_Add_Step( GeoModelCore purgebuild
  COMMAND ${CMAKE_COMMAND} -E echo "Removing previous build results for GeoModelCore"
  COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
  COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
  COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
  DEPENDEES download
  DEPENDERS patch )

add_dependencies( Package_GeoModelCore GeoModelCore )
if( ATLAS_BUILD_EIGEN )
  add_dependencies ( GeoModelCore Eigen )
endif()

# Install GeoModelCore:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

# Install its find-module:
install( FILES cmake/FindGeoModelCore.cmake
   DESTINATION ${CMAKE_INSTALL_CMAKEDIR}/modules )
