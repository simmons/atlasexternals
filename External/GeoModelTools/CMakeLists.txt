# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#
# Package building GeoModelTools for ATLAS.
#

# Set a minimum required CMake version to use.
cmake_minimum_required( VERSION 3.7 )

# Make sure that all _ROOT variables *are* used when they are set.
if( POLICY CMP0074 )
   cmake_policy( SET CMP0074 NEW )
endif()

# The name of the package:
atlas_subdir( GeoModelTools )

# In release recompilation mode finish here:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# External dependencies.
find_package( Qt5 COMPONENTS Core Sql ) # FIXME: the Qt5 is instroduced by GeoModelIO, it should be not.
find_package( nlohmann_json )

# Directory for the temporary build results:
set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/GeoModelToolsBuild )

# Extra configuration parameters.
set( _extraOptions )
if( NOT "${CMAKE_BUILD_TYPE}" STREQUAL "" )
    list( APPEND _extraOptions -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE} )
endif()
if( "${CMAKE_CXX_STANDARD}" GREATER_EQUAL 11 )
   list( APPEND _extraOptions -DCMAKE_CXX_STANDARD:STRING=${CMAKE_CXX_STANDARD} )
endif()

# List of paths given to CMAKE_PREFIX_PATH.
set( _prefixPaths ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   $ENV{CMAKE_PREFIX_PATH} ${QT5_LCGROOT} ${nlohmann_json_DIR} )
if( ( NOT ATLAS_BUILD_EIGEN ) AND EIGEN_LCGROOT )
   find_package( Eigen )
   list( APPEND _prefixPaths ${EIGEN_LCGROOT} )
endif()
if( ( NOT ATLAS_BUILD_XERCESC ) AND XERCESC_LCGROOT )
   find_package( XercesC )
   list( APPEND _prefixPaths ${XERCESC_LCGROOT} )
endif()

# Set up the build of GeoModelTools:
ExternalProject_Add( GeoModelTools
  PREFIX ${CMAKE_BINARY_DIR}
  INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
  GIT_REPOSITORY https://gitlab.cern.ch/GeoModelDev/GeoModelTools.git
  GIT_TAG 4.0.0
  CMAKE_CACHE_ARGS
  -DCMAKE_PREFIX_PATH:PATH=${_prefixPaths}
  -DCMAKE_INSTALL_LIBDIR:PATH=${CMAKE_INSTALL_LIBDIR}
  -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
  ${_extraOptions}
  LOG_CONFIGURE 1
  )
ExternalProject_Add_Step( GeoModelTools buildinstall
  COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/ <INSTALL_DIR>
  COMMENT "Installing GeoModelTools into the build area"
  DEPENDEES install  )
ExternalProject_Add_Step( GeoModelTools purgebuild
  COMMAND ${CMAKE_COMMAND} -E echo "Removing previous build results for GeoModelTools"
  COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
  COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
  COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
  DEPENDEES download
  DEPENDERS patch )
add_dependencies( GeoModelTools GeoModelCore GeoModelIO nlohmann_json )
if( ATLAS_BUILD_EIGEN )
   add_dependencies( GeoModelTools Eigen )
endif()
if( ATLAS_BUILD_XERCESC )
   add_dependencies( GeoModelTools XercesC )
endif()
add_dependencies( Package_GeoModelTools GeoModelTools )

# Install GeoModelTools:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

# Install its find-module:
install( FILES cmake/FindGeoModelTools.cmake
   DESTINATION ${CMAKE_INSTALL_CMAKEDIR}/modules )
