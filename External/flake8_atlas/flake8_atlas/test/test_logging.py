# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
from flake8_atlas.test.testutils import Flake8Test

class Test(Flake8Test):
   """
   Test proper use of loggging facilities
   """

   def test1(self):
      """Check lazy logging"""
      from flake8_atlas.checks import delayed_string_interpolation as checker

      # These tests should be checking code like
      #    log.info('logging %s' % 'bad')
      # When run within flake8 it takes care of replacing the actual strings
      # with placeholders to avoid accidental matches (here %). For the tests here
      # we simply omit the string formatter.
      #
      # http://flake8.pycqa.org/en/latest/internal/checker.html#flake8.processor.mutate_string

      self.assertFail("log.info('logging' % 'bad')", checker)
      self.assertPass("log.info('logging', 'good')", checker)

      self.assertFail("myfunnyloggername.warning('Hello' % 'world')", checker)

      self.assertFail("log.info('logging' % ('bad',42))", checker)
      self.assertPass("log.info('logging', 'good', 42)", checker)

   def test2(self):
      """Check use of print statements for logging"""
      from flake8_atlas.checks import print_for_logging as checker
      self.assertFail("print", checker)
      self.assertFail("print('Hello')", checker)
      self.assertFail("print 'Hello'", checker)

      # Those should not trigger warnings:
      self.assertPass("def myprint(): pass", checker)
      self.assertPass("def printSummary(): pass", checker)
      self.assertPass("def my_print_function(): pass", checker)
