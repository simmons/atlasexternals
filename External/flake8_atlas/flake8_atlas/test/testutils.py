# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

import unittest
import ast

class Flake8Test(unittest.TestCase):
   """Unit test case for flake8 checker"""

   def _run(self, source, checker):
      """Run AST or line-based checker and return list of errors"""
      if hasattr(checker, 'run'):
         if hasattr(source, 'read'):  # file object
            chk = checker(ast.parse(source.read()), filename=source.name).run()
         else:
            chk = checker(ast.parse(source)).run()
      else:
         chk = checker(source)
      return list(chk)

   def assertFail(self, source, checker):
      """Test that checker returns an error on source"""
      codes = self._run(source, checker)
      self.assertEqual(len(codes), 1)

   def assertPass(self, source, checker):
      """Test that checker passes on source"""
      codes = self._run(source, checker)
      self.assertListEqual(codes, [])
