AthAnalysisExternals
==============

This project builds all the externals needed by the analysis (aka miminal) offline software release of
ATLAS. It picks up most of the externals from an LCG release. Only adding
software that is either specific to ATLAS, or needs to be built in a way
specific to the experiment.
