
# VP1LightExternals


This project builds all the external packages needed by the ATLAS standalone VP1Light
project.

Many externals for VP1Light can be taken from the build system itself.
The project will not force building all of its externals by default if an
appropriate version of that external is already available on the build
machine (_e.g._, for ROOT or Boost).

## How To Build


### Get the code

```
git clone https://gitlab.cern.ch/atlas/atlasexternals.git
```

### Build and install

You can now build the source code:

```
mkdir build_ext
cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo -DCTEST_USE_LAUNCHERS=TRUE ../atlasexternals/Projects/VP1LightExternals/
make -j 4
make install DESTDIR=../install/VP1LightExternals
```

### Test the installation

You can check if the `VP1LightExternals` have been installed correctly, by sourcing the setup script (which will be used when compiling `VP1Light`; please, replace `<your-platform>` on first line with your actual platform) and check the location, _e.g._, of the `root` binary:
```
source ../install/VP1LightExternals/InstallArea/<your-platform>/setup.sh
which root
/Users/rbianchi/code_work_local/atlas/test_vp1light_mac_compile_mojave/build/install/VP1LightExternals/0.0.1/InstallArea/x86_64-mac1014-clang100-opt/bin/root
```

If you do not get errors and if you see your `build` folder in the outcome path, then the installation succeeded.

## Next Steps

You can now build `VP1Light`. For that, please refer to the instructions in `athena/Projects/VP1Light/README.md`.


## Appendix

### Building on Apple macOS

If you get errors from CMake concerning a "mismatch between PythonInterp and PythonLibs", it is probably caused by alternative versions of Python installed on your system (for example, through the [Homebrew package manager](http://brew.sh)). You can fix that by forcing CMake to use the system Python, by running:

```
cmake -DPython_EXECUTABLE:FILEPATH=/usr/bin/python ../atlasexternals/Projects/VP1LightExternals/
```


### Configuration Options


The configuration of the project respects the following options, all
defined in the `ProjectOptions.cmake` file:

   * `ATLAS_BUILD_PYTHON`: When turned on, the project builds an
   appropriate version of Python.
   * `ATLAS_BUILD_BOOST`: When turned on, the project builds an
   appropriate version of Boost.
   * `ATLAS_BUILD_DCAP`: When turned on, the project tries to build
   an appropriate version of DCAP.
   * `ATLAS_BUILD_ROOT`: When turned on, the project builds an
   appropriate version of ROOT.
   * `ATLAS_BUILD_QT`: When turned on, the project builds an
   appropriate version of Qt.

The project will try to set correct default values for these variables
based on the build environment. But they can be overridden during the
CMake configuration if necessary.


### Compilation of a single package

If you want to compile a single external package only---_e.g._, if you are modifying the configuration of the package---you can compile it with the command:

```
make Package_Coin3D
```

`Coin3D` is one of the External packages; you can compile other packages by replacing the package's name in the command, _e.g_, `make Package_ROOT` to compile the `ROOT` External package.
